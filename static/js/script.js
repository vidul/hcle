/* Author: Vidul Nikolaev Petrov
 */

$(document).ready(function () {

    var socket = io.connect();

    $('#submit').bind('click', function () {
        var data = $('#input').val();
        socket.emit('message', data);
    });

    socket.on('message', function (data) {
        $('#result').html('<i>' + data + '</i>');
    });
});
