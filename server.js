//setup Dependencies
var connect = require('connect'),
    express = require('express'),
    io = require('socket.io'),
    port = (process.env.PORT || 8081);

//Setup Express
var server = express.createServer();
server.configure(function () {
    server.set('views', __dirname + '/views');
    server.set('view options', {
        layout: false
    });
    server.use(connect.bodyParser());
    server.use(express.cookieParser());
    server.use(express.session({
        secret: "0b9n2y34az987jal234e923"
    }));
    server.use(connect.static(__dirname + '/static'));
    server.use(server.router);
});

//setup the errors
server.error(function (err, req, res, next) {
    if (err instanceof NotFound) {
        res.render('404.jade', {
            locals: {
                title: '404 - Not Found',
                description: '',
                author: '',
                analyticssiteid: 'XXXXXXX'
            },
            status: 404
        });
    }
    else {
        res.render('500.jade', {
            locals: {
                title: 'The Server Encountered an Error',
                description: '',
                author: '',
                analyticssiteid: 'XXXXXXX',
                error: err
            },
            status: 500
        });
    }
});
server.listen(port);

//Setup Socket.IO
var io = io.listen(server);
var io_data = {};
var user_limit = 3;
var init_game = function () {
    return {
        try: 0,
        number: getRandom(),
    };

    function getRandom() {
        return Math.floor(Math.random() * 10);
    }
};

io.sockets.on('connection', function (socket) {
    io_data[socket.handshake.headers.cookie] = init_game();

    socket.on('message', function (data) {
        var result, counter, user = io_data[socket.handshake.headers.cookie];

        user.try++;

        console.log('DATA >>>', data, ' USER >>>', user);

        if (user.number === +data) {
            result = 'Success: the number is ' + data + '. Restarting the game.';
            io_data[socket.handshake.headers.cookie] = init_game();
        }
        else if (user.try === user_limit) {
            result = 'Failure: ' + user_limit + ' tries, ';
            result += ' the number is ' + user.number + '. Restarting the game.';
            io_data[socket.handshake.headers.cookie] = init_game();
        }
        else {
            counter = user_limit - user.try;
            if (counter === 1) {
                result = 'There is ' + counter + ' try left.';
            }
            else {
                result = 'There are ' + counter + ' tries left.';
            }
        }

        socket.emit('message', result);
    });

    socket.on('disconnect', function () {
        console.log('Client Disconnected.');
    });
});


///////////////////////////////////////////
//              Routes                   //
///////////////////////////////////////////

/////// ADD ALL YOUR ROUTES HERE  /////////

server.get('/', function (req, res) {
    res.render('index.jade', {
        locals: {
            title: 'Your Page Title',
            description: 'Your Page Description',
            author: 'Your Name',
            analyticssiteid: 'XXXXXXX',
        }
    });
});


//A Route for Creating a 500 Error (Useful to keep around)
server.get('/500', function (req, res) {
    throw new Error('This is a 500 Error');
});

//The 404 Route (ALWAYS Keep this as the last route)
server.get('/*', function (req, res) {
    throw new NotFound;
});

function NotFound(msg) {
    this.name = 'NotFound';
    Error.call(this, msg);
    Error.captureStackTrace(this, arguments.callee);
}


console.log('Listening on http://0.0.0.0:' + port);
